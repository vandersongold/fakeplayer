package com.example.faketvplayer.player;

import com.example.faketvplayer.model.TextChannel;

public class PlayText extends Player <PlayText> {

    private TextChannel textChannel;
    private OnActionClickedListener mActionListener;

    public PlayText(PlayStrategy<PlayText> strategy, TextChannel channel, OnActionClickedListener actionListener) {
        super(strategy);
        this.textChannel = channel;
        this.mActionListener = actionListener;
    }

    public void setChannel(TextChannel channel) {
        this.textChannel = channel;
    }

    @Override
    protected PlayText getDetailedType() {
        return this;
    }

    protected  TextChannel getTextChannel() {
        return this.textChannel;
    }

    protected void notifyAction(String content) {
        mActionListener.onPlayText(content);
    }
}
