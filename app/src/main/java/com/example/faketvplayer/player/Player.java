package com.example.faketvplayer.player;

public abstract class Player  <P extends  Player<P>>{

    public interface OnActionClickedListener {
        void onPlayImage(int content);
        void onPlayText(String content);
    }

    public static final PlayStrategy<PlayText> TEXT = new PlayTextStrategy();
    public static final PlayStrategy<PlayImage> IMAGE = new PlayImageStrategy();

    protected abstract P getDetailedType();

    private PlayStrategy<P> strategy;

    Player(PlayStrategy<P> strategy) {
        this.strategy = strategy;
    }

    public void play() {
        strategy.play(getDetailedType());
    }
}

