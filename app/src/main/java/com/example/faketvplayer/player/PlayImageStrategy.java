package com.example.faketvplayer.player;

import com.example.faketvplayer.model.ImageChannel;

public class PlayImageStrategy implements  PlayStrategy<PlayImage>{

    @Override
    public void play(PlayImage playImage) {
        ImageChannel ch = playImage.getImageChannel();

        playImage.notifyAction(ch.getContent());
    }
}

