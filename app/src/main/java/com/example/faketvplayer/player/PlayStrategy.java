package com.example.faketvplayer.player;

public interface PlayStrategy <P extends Player> {
    void play(P p);
}
