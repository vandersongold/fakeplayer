package com.example.faketvplayer.player;

import com.example.faketvplayer.model.ImageChannel;

public class PlayImage extends Player <PlayImage> {

    private ImageChannel imageChannel;
    private OnActionClickedListener mActionListener;

    public PlayImage(PlayStrategy<PlayImage> strategy, ImageChannel channel, OnActionClickedListener actionListener) {
        super(strategy);
        this.imageChannel = channel;
        this.mActionListener = actionListener;
    }

    public void setChannel(ImageChannel channel) {
        this.imageChannel = channel;
    }

    @Override
    protected PlayImage getDetailedType() {
        return this;
    }

    protected  ImageChannel getImageChannel() {
        return this.imageChannel;
    }

    protected void notifyAction(int content) {
        mActionListener.onPlayImage(content);
    }
}
