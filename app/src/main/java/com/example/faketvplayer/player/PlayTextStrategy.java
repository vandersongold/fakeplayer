package com.example.faketvplayer.player;

import com.example.faketvplayer.model.TextChannel;

public class PlayTextStrategy implements  PlayStrategy<PlayText>{

    @Override
    public void play(PlayText playText) {
        TextChannel ch = playText.getTextChannel();

        playText.notifyAction(ch.getContent());
    }
}
