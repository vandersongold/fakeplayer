package com.example.faketvplayer.view;


import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.View;

import com.example.faketvplayer.R;
import com.example.faketvplayer.databinding.ActivityMainBinding;
import com.example.faketvplayer.model.ChannelType;
import com.example.faketvplayer.model.ImageChannel;
import com.example.faketvplayer.model.Response;
import com.example.faketvplayer.model.TextChannel;
import com.example.faketvplayer.viewmodel.PlayerViewModel;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    private PlayerViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDataBinding();

        setButtonsListeners();
    }

    private void initDataBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mViewModel = ViewModelProviders.of(this).get(PlayerViewModel.class);
        mViewModel.init();
        binding.setViewModel(mViewModel);
        setViewModelObserver();
    }

    private void setViewModelObserver() {
        mViewModel.getPlayResponse()
                .observe(this, response -> {
                    if (response.type == ChannelType.CHANNEL_TEXT) {
                        onChannelTextChanged(response.contentText);
                    }
                    else if (response.type == ChannelType.CHANNEL_IMAGE) {
                        onChannelImageChanged(response.contentImage);
                    }
                });
    }

    private void setButtonsListeners() {
        binding.btnChannel1.setOnClickListener(v -> mViewModel.playChannel(new TextChannel()));
        binding.btnChannel2.setOnClickListener(v -> mViewModel.playChannel(new ImageChannel()));
    }

    private void onChannelTextChanged(String content) {
        binding.imageView.setVisibility(View.INVISIBLE);
        binding.textView.setVisibility(View.VISIBLE);
        binding.textView.setText(content);
    }

    private void onChannelImageChanged(int content) {
        binding.textView.setVisibility(View.INVISIBLE);
        binding.imageView.setVisibility(View.VISIBLE);
        binding.imageView.setImageResource(content);
    }
}
