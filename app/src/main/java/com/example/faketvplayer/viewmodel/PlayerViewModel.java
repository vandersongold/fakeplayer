package com.example.faketvplayer.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.faketvplayer.model.ChannelType;
import com.example.faketvplayer.model.ImageChannel;
import com.example.faketvplayer.model.Response;
import com.example.faketvplayer.model.TextChannel;
import com.example.faketvplayer.player.PlayImage;
import com.example.faketvplayer.player.PlayText;
import com.example.faketvplayer.player.Player;

public class PlayerViewModel extends ViewModel implements Player.OnActionClickedListener {
    private MutableLiveData<Response> responseLiveData = new MutableLiveData<>();
    private PlayImage imagePlayer;
    private PlayText textPlayer;

    public void init() {
        imagePlayer = new PlayImage(Player.IMAGE, new ImageChannel(), this);
        textPlayer = new PlayText(Player.TEXT, new TextChannel(), this);
    }

    public void playChannel(TextChannel channel) {
        textPlayer.setChannel(channel);
        textPlayer.play();
    }

    public void playChannel(ImageChannel channel) {
        imagePlayer.setChannel(channel);
        imagePlayer.play();
    }

    public LiveData<Response> getPlayResponse() {
        if (responseLiveData == null) {
            responseLiveData = new MutableLiveData<>();
        }
        return responseLiveData;
    }

    @Override
    public void onPlayImage(int content) {
        responseLiveData.setValue(new Response(content, ChannelType.CHANNEL_IMAGE));
    }

    @Override
    public void onPlayText(String content) {
        responseLiveData.setValue(new Response(content, ChannelType.CHANNEL_TEXT));
    }
}

