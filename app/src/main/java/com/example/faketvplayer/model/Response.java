package com.example.faketvplayer.model;

public class Response {
    public String contentText;
    public int contentImage;
    public ChannelType type;

    public Response(String content, ChannelType type) {
        this.contentText = content;
        this.type = type;
    }

    public Response(int content, ChannelType type) {
        this.contentImage = content;
        this.type = type;
    }
}
