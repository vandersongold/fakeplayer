package com.example.faketvplayer.model;

import com.example.faketvplayer.R;

public class ImageChannel extends Channel<Integer> {
    private static final  int content  =  R.drawable.houseofcards;

    @Override
    public Integer getContent() {
        return content;
    }
}
