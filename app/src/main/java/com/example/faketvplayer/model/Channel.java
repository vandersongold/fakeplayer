package com.example.faketvplayer.model;


public abstract class Channel<T> {

    public abstract T getContent();
}
