package com.example.faketvplayer.model;

public class TextChannel extends Channel<String>  {
    private static final String content  = "Streaming a text..";

    @Override
    public String getContent() {
        return content;
    }
}
