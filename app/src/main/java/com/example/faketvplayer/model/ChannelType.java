package com.example.faketvplayer.model;

public enum ChannelType {
    CHANNEL_TEXT,
    CHANNEL_IMAGE
}
