package com.example.faketvplayer;

import android.arch.core.executor.testing.InstantTaskExecutorRule;

import com.example.faketvplayer.model.ImageChannel;
import com.example.faketvplayer.model.TextChannel;
import com.example.faketvplayer.player.PlayImage;
import com.example.faketvplayer.player.PlayText;
import com.example.faketvplayer.player.Player;


import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static org.junit.Assert.*;


public class PlayerUnitTest implements Player.OnActionClickedListener {
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private ImageChannel imageChannel;
    private TextChannel textChannel;

    @Before
    public void setUp() {
        imageChannel = new ImageChannel();
        textChannel = new TextChannel();
    }

    @Test
    public void playTextChannelCorrectly() {
        Player text = new PlayText(Player.TEXT, new TextChannel(), this);
        text.play();
    }

    @Test
    public void playImageChannelCorrectly() {
        Player text = new PlayImage(Player.IMAGE, new ImageChannel(), this);
        text.play();
    }

    @Override
    public void onPlayImage(int content) {
        assertEquals(content, (int)imageChannel.getContent());
    }

    @Override
    public void onPlayText(String content) {
        assertEquals(content, textChannel.getContent());
    }
}