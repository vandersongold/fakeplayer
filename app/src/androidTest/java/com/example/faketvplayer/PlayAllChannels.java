package com.example.faketvplayer;

import android.support.test.rule.ActivityTestRule;

import com.example.faketvplayer.model.ImageChannel;
import com.example.faketvplayer.model.TextChannel;
import com.example.faketvplayer.view.MainActivity;

import org.junit.Rule;
import org.junit.Test;

import static com.schibsted.spain.barista.assertion.BaristaImageViewAssertions.assertHasAnyDrawable;
import static com.schibsted.spain.barista.assertion.BaristaImageViewAssertions.assertHasDrawable;
import static com.schibsted.spain.barista.assertion.BaristaImageViewAssertions.assertHasNoDrawable;
import static com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed;
import static com.schibsted.spain.barista.interaction.BaristaClickInteractions.clickOn;

public class PlayAllChannels {
    @Rule
    public ActivityTestRule<MainActivity> activityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testPlayTextChannel() {
        clickOn(R.id.btnChannel1);

        TextChannel channel = new TextChannel();

        assertDisplayed(R.id.textView);
        assertDisplayed(channel.getContent());

        // just confirm that image is not displayed at this moment
        assertHasNoDrawable(R.id.imageView);
    }

    @Test
    public void testPlayImageChannel() {
        clickOn(R.id.btnChannel2);

        assertHasAnyDrawable(R.id.imageView);
        assertHasDrawable(R.id.imageView, R.drawable.houseofcards);
    }
}
